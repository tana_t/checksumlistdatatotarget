package com.csl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.csl.Bo.RequestData;
import com.csl.service.Testservice;

@SpringBootTest
class Demo1ApplicationTests {
	@Autowired
	private Testservice testservice;

	@Test
	void testTrue5() {
		RequestData data = new RequestData();
		ArrayList<Integer> ss = new ArrayList<>();
		ss.add(1);
		ss.add(2);
		ss.add(3);
		ss.add(4);
		ss.add(5);
		ss.add(6);
		data.setDatalist(ss);
		data.setData(11);
		List l = testservice.testserviceq(data);

		System.out.println(l);
	}

	@Test
	void testTrue6() {
		RequestData data = new RequestData();
		ArrayList<Integer> ss = new ArrayList<>();
		ss.add(2);
		ss.add(6);
		ss.add(3);
		data.setDatalist(ss);
		data.setData(6);
		List l = testservice.testserviceq(data);
		System.out.println(l);
	}
}
