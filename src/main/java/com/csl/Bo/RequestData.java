package com.csl.Bo;

import java.util.ArrayList;

public class RequestData {

	private ArrayList<Integer> datalist;

	private Integer data;

	public ArrayList<Integer> getDatalist() {
		return datalist;
	}

	public void setDatalist(ArrayList<Integer> datalist) {
		this.datalist = datalist;
	}

	public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}

}
