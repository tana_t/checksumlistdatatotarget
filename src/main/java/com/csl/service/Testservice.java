package com.csl.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.csl.Bo.RequestData;

@Service
public class Testservice {

	public List<List<Integer>> testserviceq(RequestData data) {
		List<Integer> datalistAll = data.getDatalist();
		List<List<Integer>> totalDataLists = new ArrayList<>();
		for (Integer integer : datalistAll) {
			Integer datacout1 = data.getData() / integer;
			Integer datacout2 = data.getData() % integer;

			if (datacout2 != 0) {
				List<Integer> arrayLists = new ArrayList<>();
				for (Integer datas : datalistAll) {
					if ((datas + integer) == data.getData()) {
						arrayLists.add(integer);
						arrayLists.add(datas);
					}
				}
				if (arrayLists.size() > 0) {
					Collections.sort(arrayLists);
					// Check duplicate.
					boolean isdup = false;
					for (List<Integer> l : totalDataLists) {
						isdup = arrayLists.containsAll(l);
					}
					//
					if (!isdup) {
						totalDataLists.add(arrayLists);
					}
				}
			} else {
				List<Integer> arrayLists = new ArrayList<>();
				for (int i = 0; i < datacout1; i++) {
					arrayLists.add(integer);
				}
				totalDataLists.add(arrayLists);
			}
		}

		return totalDataLists;
	}

}
