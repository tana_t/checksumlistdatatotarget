package com.csl.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.csl.Bo.RequestData;
import com.csl.service.Testservice;

@RestController
public class test {

	@Autowired
	private Testservice testservice;

	@PostMapping("/test")
	public List<List<Integer>> getLoanAccount(@RequestBody RequestData data) {
		return testservice.testserviceq(data);
	}
}
